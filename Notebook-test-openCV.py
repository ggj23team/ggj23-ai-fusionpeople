#!/usr/bin/env python
# coding: utf-8

# In[107]:


from __future__ import print_function
import cv2
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

from IPython.display import Image
import os
import sys

#get_ipython().run_line_magic('matplotlib', 'inline')




# Read images from the directory.
def readImages(path):
    print("Reading images from " + path, end = "...")
    # Create array of array of images.
    images = []
    # List all files in the directory and read points from text files one by one.
    for filePath in sorted(os.listdir(path)):
        fileExt = os.path.splitext(filePath)[1]
        if fileExt in [".jpg", ".jpeg"]:

            # Add to array of images.
            imagePath = os.path.join(path, filePath)
            im = cv2.imread(imagePath)

            if im is None :
                print("image:{} not read properly".format(imagePath))
            else :
                # Convert image to floating point.
                im = np.float32(im)/255.0
                # Add image to list.
                images.append(im)
                # Flip image.
                imFlip = cv2.flip(im, 1);
                # Append flipped image.
                images.append(imFlip)
    numImages = int(len(images) / 2)
    # Exit if no image found.
    if numImages == 0 :
        print("No images found")
        sys.exit(0)

    print(str(numImages) + " files read.")
    return images


# Create data matrix from a list of images.
def createDataMatrix(images):
    print("Creating data matrix", end = " ... ")

    numImages = len(images)
    sz = images[0].shape
    # Data matrix.
    data = np.zeros((numImages, sz[0] * sz[1] * sz[2]), dtype = np.float32)
    for i in range(0, numImages):
        image = images[i].flatten()
        # Each row get replaced with one flattened image.
        data[i,:] = image

    print("DONE")
    return data


def createNewFace(*args):
    # Start with the mean image.
    output = averageFace

    # Add the eigen faces with the weights.
    for i in range(0, NUM_EIGEN_FACES):
        # Get trackbar position.
        sliderValues[i] = cv2.getTrackbarPos("Weight" + str(i), "Trackbars");
        weight = sliderValues[i] - MAX_SLIDER_VALUE/2
        # Add the weighted eigen face to the mean face.
        output = np.add(output, eigenFaces[i] * weight)

    # Display Result at 2x size.
    output = cv2.resize(output, (0,0), fx = 2, fy = 2)
    cv2.imshow("Result", output)


if __name__ == '__main__':

    # Number of EigenFaces.
    NUM_EIGEN_FACES = 3

    # Maximum weight.
    MAX_SLIDER_VALUE = 255
    

    face_cascade = cv2.CascadeClassifier("Classifiers/haarcascade_frontalface_default.xml")
    eye_cascade = cv2.CascadeClassifier("Classifiers/haarcascade_eye.xml")
    nose_cascade = cv2.CascadeClassifier("Classifiers/haarcascade_mcs_nose.xml")
    mouth_cascade = cv2.CascadeClassifier("Classifiers/haarcascade_mcs_mouth.xml")


    # Directory containing images.
    dirName = "samplesNOBG"

    # Read images.
    images = readImages(dirName)

    # Size of images.
    sz = images[0].shape

    # Create data matrix for PCA.
    data = createDataMatrix(images)

    # Compute the eigenvectors from the stack of images created.
    print("Calculating PCA ", end="...")
    
    mean, eigenVectors = cv2.PCACompute(data, mean = None, maxComponents = NUM_EIGEN_FACES)
    
    print("DONE")

    averageFace = mean.reshape(sz)

    # Create a container to hold eigen faces.
    eigenFaces  = []

    # Reshape eigen vectors to eigen faces.
    for eigenVector in eigenVectors:
        # REshape.
        eigenFace = eigenVector.reshape(sz)
        # Append eigen faces to the container.
        eigenFaces.append(eigenFace)

    # Create window for displaying result.
    cv2.namedWindow("Result", cv2.WINDOW_NORMAL)
    # Create window for displaying mean face.
    cv2.namedWindow("Average", cv2.WINDOW_NORMAL)

    # Upscale by a factor of two.
    output = cv2.resize(averageFace, (0,0), fx = 2, fy = 2)
    
    # Display.
    cv2.imshow("Result", output)
    cv2.imshow("Average", averageFace)



    # Create Window for trackbars.
    cv2.namedWindow("Trackbars", cv2.WINDOW_NORMAL)

    # Create a list to contain slider values.
    #sliderValues = []

    # Create Trackbars.
    #for i in range(0, NUM_EIGEN_FACES):
    #    sliderValues.append(int(MAX_SLIDER_VALUE/2))
    #    cv2.createTrackbar( "Weight" + str(i), "Trackbars", int(MAX_SLIDER_VALUE/2), MAX_SLIDER_VALUE, createNewFace)

    # You can reset the sliders by clicking on the mean image.
    #cv2.setMouseCallback("Average", resetSliderValues);

    #print('''Usage:
    #Change the weights using the sliders.
    #Mouse hover on the result window to reset sliders.
    #Press q to terminate.''')

    key = cv2.waitKey(0)
    if key == ord('q'):
        cv2.destroyAllWindows()



    cv2.imwrite("test.png", 255*output)


    faceCascade = face_cascade

    rgb = cv2.imread('test.png')
    gray = cv2.cvtColor(rgb, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=25,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    x, y, w, h = faces[0]  # Working with image with only one face
    imh, imw = gray.shape

    center_x, center_y = int(x+w/2), int(y+h/2)

    mask = np.zeros((imh, imw), np.uint8)
    cv2.ellipse(mask, (center_x, center_y), (int(w/2), int(h/2)), 0, 0, 360, 255, cv2.FILLED)
    rgb[mask == 0] = 255
    plt.imshow(rgb[y:y+h, x:x+w])

    cv2.imwrite("out.png", rgb)



